<?php

  class ThreeTwentyNYAffiliate_Actions extends Cms_ActionScope
  {
    /*
     *    Session Actions
     */
    public function signup()
    {
      // Set Up Need Variables
      $countries = Shop_Country::create()->find_all();
      $this->data['countries'] = $countries;

      $states = Shop_CountryState::create()->find_all();
      $this->data['states'] = $states;

      if (post('login')) {
        $shop_actions = new Shop_Actions();
        $shop_actions->on_login();
      } elseif (post('signup')) {
        $this->on_signup();
      }
    }

    public function on_signup()
    {
    try
      {
        // Custom signup for Affiliates
        // ... copied dirrectly from Shop_Actions on_signup() lines 834 - 839
        $customer = new Shop_Customer();
        $customer->disable_column_cache('front_end', false);

        $customer->init_columns_info('front_end');
        $customer->validation->focusPrefix = null;
        $customer->validation->getRule('email')->focusId('customer_signup_email');
        // ... end copy
        
        if (!post_array_item('customer', 'password'))
          $customer->generate_password();

        // Assign Customer to our affiliate group
        $affiliate_group = Shop_CustomerGroup::find_by_code('threetwentynyaffiliate');
        if (!$affiliate_group)
          throw new Cms_Exception('Could not find Affiliate customer group. Please add a group with an API code of "threetwentynyaffiliate".');

        $customer->customer_group_id = $affiliate_group->id;

        $customer->save(post('customer', array()));

        // We need to create an associated Affiliate for this user
        $affiliate = new ThreeTwentyNYAffiliate_Affiliate();
        $affiliate->payment_first_name = $customer->first_name;
        $affiliate->payment_last_name = $customer->last_name;
        $affiliate->payment_email = $customer->email;
        $affiliate->owner_id = $customer->id;

        // We will generate the affiliate name from the customer name if we do not see the
        // affiliate[name] form field in the recieved $_POST
        if (!post_array_item('affiliate', 'name'))
          $affiliate->name = $customer->first_name . ' ' . $customer->last_name;

        $affiliate->save(post('affiliate', array()));
      }
      catch (Exception $ex)
      {
        // If validation fails we need to ensure the $customer gets deleted.
        // Without this we could have a bunch of random user records.
        $customer->delete();
        throw $ex;
      }

      /*
           Send Confirmation email
            - should be extracted down to a model
      */
      $template = System_EmailTemplate::create()->find_by_code('threetwentynyaffiliate:registration_confirmation');
      if ($template && $customer)
      {
        $template->subject = $customer->set_customer_email_vars($template->subject);
        $message = $customer->set_customer_email_vars($template->content);
        $message = str_replace('{affiliate_url}', $affiliate->url, $message);
        $template->send_to_customer($customer, $message);
      }
      // End Email
      
      // ... copied dirrectly from Shop_Actions on_signup() lines 861 - 860
      if (post('flash'))
        Phpr::$session->flash['success'] = post('flash');
      
      if (post('customer_auto_login'))
        Phpr::$frontend_security->customerLogin($customer->id);
       
      $redirect = post('redirect');
      if ($redirect)
        Phpr::$response->redirect($redirect);
    }

    /* 
     *    Affiliate Account Action
     */
    public function affiliate() 
    {
      $affiliates = ThreeTwentyNYAffiliate_Affiliate::create();
      $affiliate = $affiliates->where("owner_id = ?", $this->customer->id)->find();

      $this->data['affiliate'] = $affiliate;

    }
  }
