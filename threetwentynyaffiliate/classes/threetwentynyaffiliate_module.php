<?php

  class ThreeTwentyNYAffiliate_Module extends Core_ModuleBase
  {
    protected function createModuleInfo()
    {
      return new Core_ModuleInfo(
        "Affiliate Program",
        "Adds and an affiliate program to your store",
        "320ny",
        "http://320ny.com"
      );
    }

    public function listTabs($tabCollection)
    {
      $menu_item = $tabCollection->tab('threetwentynyaffiliate', 'Affiliate Program', 'affiliates', 90);
      $menu_item->addSecondLevel('affiliates', 'Affiliates', 'affiliates');
      $menu_item->addSecondLevel('payouts', 'Payouts', 'payouts');
    }

    /* 
        Settings
    */
    public function listSettingsItems()
    {
      return array(
        array(
          'icon'=>'/modules/threetwentynyaffiliate/resources/images/settings-logo.png',
          'title'=>'Settings',
          'url'=>'/threetwentynyaffiliate/settings',
          'description'=>'Configure commission and cookie settings.',
          'sort_id'=>90,
          'section'=>'Affiliate Program'
        )
      );
    }

    /*
        Registering LemonStand Events
    */
    public function subscribeEvents()
    {
      Backend::$events->addEvent('cms:onBeforeDisplay', $this, 'before_page_display');
      Backend::$events->addEvent('shop:onNewOrder', $this, 'on_new_order');
    }

    // Event Functions
    public function before_page_display($page)
    {
      $token = Phpr::$request->getField('affiliate_token', false);
      // If our token is found
      if ($token) {
        //  -> Place cookie if token is present
        $settings = ThreeTwentyNYAffiliate_Setting::get();
        Phpr::$response->setCookie('threetwentynyaffiliate_token', $token, $settings->cookie_expiration_days);
      }
    }

    public function on_new_order($order_id)
    {
      // Find Affiliate
      $affiliate = ThreeTwentyNYAffiliate_Helper::current_affiliate();
      // Find Order
      $order = Shop_Order::create()->find($order_id);
      // Create Affiliate Order
      if ($affiliate && $order) {
        $obj = new ThreeTwentyNYAffiliate_Referral();
        $obj->affiliate_id = $affiliate->id;
        $obj->order_id = $order->id;

        if ($affiliate->commission_override_syetem_defaults) {
          // User the Affiliate settings
          $obj->commission_method = $affiliate->commission_method;
          $obj->commission_percentage = $affiliate->commission_percentage;
          $obj->commission_flat_rate = $affiliate->commission_flat_rate;
        } else {
          // Access System Settings
          $settings = ThreeTwentyNYAffiliate_Setting::get();
          $obj->commission_method = $settings->commission_method;
          $obj->commission_percentage = $settings->commission_percentage;
          $obj->commission_flat_rate = $settings->commission_flat_rate;
        }
        $obj->save();
      } 
    }

    public function listEmailVariables()
    {
      return array(
        'Affiliate Program variables'=>array(
          'affiliate_url'=>array("Outputs an affiliate's unique url", "")
        )
      );
    }
 
  }
