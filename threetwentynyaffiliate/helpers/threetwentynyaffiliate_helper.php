<?php

  class ThreeTwentyNYAffiliate_Helper
  {
    public static function current_affiliate() 
    {
      $token = Phpr::$request->cookie('threetwentynyaffiliate_token');
      if ($token) {
        $affiliate = ThreeTwentyNYAffiliate_Affiliate::create()->find_by_token($token);
        if ($affiliate) {
          return $affiliate;
        } else {
          return false;
        }
      }
    } 
  }
