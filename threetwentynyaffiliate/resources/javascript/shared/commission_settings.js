jQuery( document ).ready(function( $ ) {
  var method, percentage, flat_rate;
  // System Settings Page
  if ($('#ThreeTwentyNYAffiliate_Setting_commission_method').length) {
    method = $('#ThreeTwentyNYAffiliate_Setting_commission_method'); 
    percentage = $('#form_field_commission_percentageThreeTwentyNYAffiliate_Setting');
    flat_rate = $('#form_field_commission_flat_rateThreeTwentyNYAffiliate_Setting');
  // Affiliate Edit Page
  } else if ($('#ThreeTwentyNYAffiliate_Affiliate_commission_method').length) {
    method = $('#ThreeTwentyNYAffiliate_Affiliate_commission_method');
    percentage = $('#form_field_commission_percentageThreeTwentyNYAffiliate_Affiliate');
    flat_rate = $('#form_field_commission_flat_rateThreeTwentyNYAffiliate_Affiliate');
  // Affiliate Order Edit Page
  } else {
    method = $('#ThreeTwentyNYAffiliate_Referral_commission_method');
    percentage = $('#form_field_commission_percentageThreeTwentyNYAffiliate_Referral');
    flat_rate = $('#form_field_commission_flat_rateThreeTwentyNYAffiliate_Referral');
  }
  
  // Attach a handler to method select element
  method.bind("change", display_selected_method ); 

  function display_selected_method() {
    if (method.val() == 'percentage') {
      percentage.fadeIn(1000);
      flat_rate.hide();
    } else {
      flat_rate.fadeIn(1000);
      percentage.hide();
    }
  }

  // Run once right away
  display_selected_method();
});
