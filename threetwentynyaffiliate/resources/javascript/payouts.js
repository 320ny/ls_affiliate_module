//
// Toolbar Button Actions
//
function mass_generate_payouts()
{
  new PopupForm('index_onLoadMassGeneratePayoutsForm',{});
  return false;
}

function payouts_change_status()
{
  if (!payouts_selected())
  {
    alert('Please select payouts to change status.');
    return false;
  }
  new PopupForm('index_onLoadChangeStatusForm', {
    ajaxFields: $('listThreeTwentyNYAffiliate_Payouts_index_list_body').getForm()
  });
  return false;
}

function payouts_selected()
{
  return $('listThreeTwentyNYAffiliate_Payouts_index_list_body').getElements('tr td.checkbox input').some(function(element){return element.checked});
}

//
// List Link Selection Actions
//
function select_payouts(e, select_type)
{
  var e = new Event(e);

  switch (select_type)
  {
    case 'all':
      $('listThreeTwentyNYAffiliate_Payouts_index_list_body').getElements('tr td.checkbox input').each(function(element){element.cb_check();})
      break;
    case 'none':
      deselect_payouts();
      break;
    default: // paid and unpaid
      if (!e.shift)
        deselect_payouts();
      $('listThreeTwentyNYAffiliate_Payouts_index_list_body').getElements('tr.payout_status_' +select_type+ ' td.checkbox input').each(function(element){
        element.cb_check();
      });
      break;
  }
  return false;
}

function deselect_payouts()
{
  $('listThreeTwentyNYAffiliate_Payouts_index_list_body').getElements('tr td.checkbox input').each(function(element){element.cb_uncheck();})
}
