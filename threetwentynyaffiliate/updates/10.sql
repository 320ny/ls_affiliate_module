alter table threetwentynyaffiliate_affiliates
add column commission_method varchar(20) default 'percentage',
add column commission_percentage decimal(5,2) default '0.0',
add column commission_flat_rate decimal(15,2) default '0.0',
add column commission_override_syetem_defaults tinyint(4); 
