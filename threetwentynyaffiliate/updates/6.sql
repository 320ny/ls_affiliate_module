CREATE TABLE `threetwentynyaffiliate_settings` (
  `id` int(11) NOT NULL auto_increment,
  `commission_method` varchar(20) NOT NULL,
  `commission_percentage` decimal(5,2) NOT NULL default '0.0',
  `commission_flat_rate` decimal(15,2) NOT NULL default '0.0',
  `updated_at` datetime default NULL,
  `updated_user_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

insert into threetwentynyaffiliate_settings(commission_method) values ('percentage');
