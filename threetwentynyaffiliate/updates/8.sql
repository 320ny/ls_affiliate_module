alter table threetwentynyaffiliate_affiliate_orders
add column commission_method varchar(20) default 'percentage',
add column commission_flat_rate decimal(15,2) default '0.0'; 
