insert into system_email_templates(code, subject, content, description, is_system) values("threetwentynyaffiliate:registration_confirmation", "Welcome to the Affiliate Program", 
	"<p>Hi&nbsp;{customer_first_name},</p><p>Thank you for registering for our affiliate program. Here is your affiliate link:</p><p>{affiliate_url}</p> 
	<p>Use this link to direct customers to our site and start generating commissions!</p><p>Please use the following email and password to login and manage your affiliate account:</p>
	<p><strong>Email:</strong>&nbsp;{customer_email}<br /><strong>Password:</strong>&nbsp;{customer_password}</p><p>Thank you.</p>", 
	"This message is sent to an afiliate after successful registration.", 0);
