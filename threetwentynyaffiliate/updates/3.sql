CREATE TABLE `threetwentynyaffiliate_affiliate_orders` (
  `id` int(11) NOT NULL auto_increment,
  `affiliate_id` int(11) default NULL,
  `order_id` int(11) default NULL,
  `commission_percentage` decimal(5,2) default NULL,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  `created_user_id` int(11) default NULL,
  `updated_user_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
