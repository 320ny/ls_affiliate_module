create table `threetwentynyaffiliate_payouts` (
  `id` int(11) NOT NULL auto_increment,
  `affiliate_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL default 'unpaid',
  `amount_paid` decimal(15,2) default NULL,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  `created_user_id` int(11) default NULL,
  `updated_user_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

alter table threetwentynyaffiliate_affiliate_orders
add column payout_id int(11) default NULL;
