alter table threetwentynyaffiliate_affiliates
add column payment_first_name varchar(100) default NULL,
add column payment_last_name varchar(100) default NULL,
add column payment_email varchar(100) default NULL,
add column payment_phone varchar(100) default NULL,
add column payment_company varchar(100) default NULL,
add column payment_street_addr varchar(255) default NULL,
add column payment_city varchar(100) default NULL,
add column payment_state_id int(11) default NULL,
add column payment_zip varchar(20) default NULL,
add column payment_country_id int(11) default NULL,
add column payment_paypal_email varchar(100) default NULL;
