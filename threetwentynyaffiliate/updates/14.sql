alter table threetwentynyaffiliate_payouts
add column payment_method varchar(20) NOT NULL default 'check';

alter table threetwentynyaffiliate_settings
add column payout_payment_method varchar(20) NOT NULL default 'check';  
