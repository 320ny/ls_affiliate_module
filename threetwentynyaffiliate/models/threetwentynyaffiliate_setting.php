<?php
  class ThreeTwentyNYAffiliate_Setting extends Db_ActiveRecord
  {
    public $table_name = 'threetwentynyaffiliate_settings';
    public static $loadedInstance = null;

    public $has_and_belongs_to_many = array(
      'order_statuses'=>array('class_name'=>'Shop_OrderStatus', 'join_table'=>'threetwentynyaffiliate_settings_order_statuses', 
      'order'=>'name', 'foreign_key'=>'order_status_id', 'primary_key'=>'setting_id')
    );

    public static function create($values = null)
    {
      return new self($values);
    }

    public static function get()
    {
      if (self::$loadedInstance)
        return self::$loadedInstance;

      return self::$loadedInstance = self::create()->order('id desc')->find();
    }

    public function define_columns($context = null)
    {
      $this->define_column('commission_method', 'Method');
      $this->define_column('commission_percentage', 'Percentage')->validation()->fn('trim')->required('Please specify a percentage')->method('validates_commission_percentage');
      $this->define_column('commission_flat_rate', 'Flat Rate')->validation()->fn('trim')->required('Please specify a flat rate');
      $this->define_column('payout_minimum', 'Payout Minimum')->validation()->fn('trim')->required('Please specify a payout minimum');
      $this->define_column('payout_payment_method', 'Payment Method')->validation()->fn('trim')->required('Please specify a payment method');
      $this->define_column('cookie_expiration_days', 'Cookie Expiration in Days')->validation()->fn('trim')->required('Please specify the number of days you want affiliate cookies to expire');
      $this->define_multi_relation_column('order_statuses', 'order_statuses', 'Order Statuses', '@name');
    }

    public function define_form_fields($context = null)
    {
      // Commission
      $this->add_form_field('commission_method')->tab('Commission Settings')->renderAs(frm_dropdown);
      $this->add_form_field('commission_percentage')->tab('Commission Settings')->comment("The percentage 
        you want multiplied to referred order totals to determine your affiliates commission. Changing this number
        will only affect future referral orders.<br /><br />(Commission Percentage / 100) * Order Total = 
        Affiliates Commission", "above", true);
      $this->add_form_field('commission_flat_rate')->tab('Commission Settings')->comment("Simply apply a flat rate commission for
        each referred order. Changing this number will only affect future referral orders.", "above", true);
      $this->add_form_field('order_statuses')->tab('Commission Settings')->comment("When an order is of one of these selected statuses
        the order will count torwards an affiliates commission.", "above", true);
      // Payout
      $this->add_form_field('payout_minimum')->tab('Payouts')->comment("The minimum amount an  affiliate must accumulate to generate a payout");
      $this->add_form_field('payout_payment_method')->tab('Payouts')->renderAs(frm_dropdown)->comment("The method you are going to pay affiliates their commissions");
      // Cookie
      $this->add_form_field('cookie_expiration_days')->tab('Cookie')->comment("The number of days that an affiliate cookie will last
        in a customers browser from the date they orginally clicked the affiliat's link.", "above", true);
    }

    // Validations
    public function validates_commission_percentage($name, $value)
    {
      if ($value > 100.0 || $value < 0)
        $this->validation->setError('Percentage must be between 0 and 100.', $name, true);

      return $value;
    }

    // Methods
    public function order_status_ids() 
    {
      $ids = array();
      foreach($this->order_statuses as $status) {
        array_push($ids, $status->id);
      }
      return implode(',', $ids);
    }

    // Form methods
    public function get_commission_method_options($key_index = -1)
    {
      return array(
        'percentage'=>'Percentage',
        'flat_rate'=>'Flat Rate'
      );
    }

    public function get_payout_payment_method_options($key_index = -1)
    {
      return array(
        'check'=>'Check',
        'paypal'=>'PayPal'
      );
    }
    
  }
