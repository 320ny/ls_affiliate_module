<?php

  class ThreeTwentyNYAffiliate_Affiliate extends Db_ActiveRecord
  {
    public $table_name = 'threetwentynyaffiliate_affiliates'; 

    // For created_at and updated_at fields
    public $implement = 'Db_AutoFootprints';
    public $auto_footprints_visible = true;
    
    public $has_many = array(
      'referrals'=>array('class_name'=>'ThreeTwentyNYAffiliate_Referral', 'foreign_key'=>'affiliate_id', 'order'=>'created_at desc', 'delete'=>true),
      'payouts'=>array('class_name'=>'ThreeTwentyNYAffiliate_Payout', 'foreign_key'=>'affiliate_id', 'order'=>'created_at desc', 'delete'=>true)
    );

    public $belongs_to = array(
      'owner'=>array('class_name'=>'Shop_Customer', 'foreign_key'=>'owner_id'),
      'payment_country'=>array('class_name'=>'Shop_Country', 'foreign_key'=>'payment_country_id'),
      'payment_state'=>array('class_name'=>'Shop_CountryState', 'foreign_key'=>'payment_state_id')
    );

    public static function create()
    {
      return new self();
    }

    // SCHEMA CONFIGURATION
    public $custom_columns = array('order_count'=>db_number, 'total_commission'=>db_float, 'url'=>db_text, 'link'=>db_text);

    public function define_columns($context = null)
    {
      $this->define_column('id', '#')->defaultInvisible();
      $this->define_column('name', 'Name')->validation()->fn('trim')->
        required('Please specify an affiliate name')->
        minLength(3, 'The affiliate name should be at least 3 characters in length')->
        unique('The name "%s" is already used by another affiliate.');
      $this->define_column('order_count', 'Referrals')->type(db_varchar);
      $this->define_column('total_commission', 'Total Commission')->currency(true);
      $this->define_column('token', 'Token')->invisible();
      $this->define_column('url', 'Public URL')->invisible();
      $this->define_column('link', 'Public Link')->invisible();
      if ($context == 'preview') {
        $this->define_column('total_commission', 'Total Commission')->currency(true)->type(db_varchar);
      }
      $this->define_column('commission_method', 'Commission Method')->invisible();
      $this->define_column('commission_percentage', 'Percentage')->invisible()->validation()->fn('trim')->required('Please specify a percentage')->method('validates_commission_percentage');
      $this->define_column('commission_flat_rate', 'Flat Rate')->invisible()->validation()->fn('trim')->required('Please specify a flat rate');
      $this->define_column('commission_override_syetem_defaults', 'Override System Defaluts')->defaultInvisible();

      $this->define_column('payment_first_name', 'First Name')->defaultInvisible()->validation()->fn('trim');
      $this->define_column('payment_last_name', 'Last Name')->defaultInvisible()->validation()->fn('trim');
      $this->define_column('payment_email', 'Email')->defaultInvisible()->validation()->fn('trim');
      $this->define_column('payment_phone', 'Phone')->defaultInvisible()->validation()->fn('trim');
      $this->define_column('payment_company', 'Company')->defaultInvisible()->validation()->fn('trim');
      $this->define_relation_column('payment_country', 'payment_country', 'Country ', db_varchar, '@name')->defaultInvisible();
      $this->define_relation_column('payment_state', 'payment_state', 'State ', db_varchar, '@name')->defaultInvisible();
      $this->define_column('payment_street_addr', 'Street Address')->defaultInvisible()->validation()->fn('trim');
      $this->define_column('payment_city', 'City')->defaultInvisible()->validation()->fn('trim');
      $this->define_column('payment_zip', 'Zip/Postal Code')->defaultInvisible()->validation()->fn('trim');
      $this->define_column('payment_paypal_email', 'PayPal Email')->defaultInvisible()->validation()->fn('trim');

      $this->define_relation_column('owner', 'owner', 'Owner', db_varchar, '@email')->defaultInvisible();
    }

    // FORM
    public function define_form_fields($context = null)
    {
      $this->add_form_field('name')->tab('Affiliate Details');

      $this->add_form_field('payment_first_name', 'left')->tab('Payment Information');
      $this->add_form_field('payment_last_name', 'right')->tab('Payment Information');
      $this->add_form_field('payment_email')->tab('Payment Information');
      $this->add_form_field('payment_company', 'left')->tab('Payment Information');
      $this->add_form_field('payment_phone', 'right')->tab('Payment Information');
      $this->add_form_field('payment_country', 'left')->tab('Payment Information');
      $this->add_form_field('payment_state', 'right')->tab('Payment Information');
      $this->add_form_field('payment_street_addr')->tab('Payment Information')->nl2br(true);
      $this->add_form_field('payment_city', 'left')->tab('Payment Information');
      $this->add_form_field('payment_zip', 'right')->tab('Payment Information');
      $this->add_form_field('payment_paypal_email')->tab('Payment Information');

      if ($context == 'preview') {
        $this->add_form_field('url')->tab('Affiliate Details');;
        $this->add_form_field('link')->tab('Affiliate Details');
        if ($this->commission_override_syetem_defaults) {
          $this->add_form_field('commission_method')->tab('Commission Settings');
          if ($this->commission_method == 'percentage') {
            $this->add_form_field('commission_percentage')->tab('Commission Settings');
          } else {
            $this->add_form_field('commission_flat_rate')->tab('Commission Settings');
          }
          $this->add_form_field('commission_override_syetem_defaults')->tab('Commission Settings');
        }
      } else {
        $this->add_form_field('commission_method')->tab('Commission Settings')->renderAs(frm_dropdown);
        $this->add_form_field('commission_percentage')->tab('Commission Settings');
        $this->add_form_field('commission_flat_rate')->tab('Commission Settings');
        $this->add_form_field('commission_override_syetem_defaults')->tab('Commission Settings')
          ->comment("If this option is selected all future referrals for this affiliate will inherit these settings. 
                     If not selected future referrals will inherit from the commission method and rates set in the Affiliate Program Settings under System settings.");
      }
    }

    // Virtual Attributes
    public function eval_url() {
      $url = site_url('/') . '?affiliate_token=' . $this->token;
      return $url;
    }

    public function eval_link() {
      return '<a href="' . $this->url .'" />Click Here</a>';
    }

    public function eval_order_count() {
      return $this->qualified_referrals()->count();
    }

    public function eval_total_commission() {
      $total = 0.0;
      foreach($this->qualified_referrals() as $refered_order) {
        $total = $total + $refered_order->commission;
      }
      return $total;
    }

    // Validations
    public function validates_commission_percentage($name, $value)
    {
      if ($value > 100.0 || $value < 0)
        $this->validation->setError('Percentage must be between 0 and 100.', $name, true);

      return $value;
    }

    // CALLBACKS
    public function before_validation_on_create($deferred_session_key = null)
    {
      if (!$this->commission_percentage)
        $this->commission_percentage = 0.0;
      if (!$this->commission_flat_rate)
        $this->commission_flat_rate = 0.0;
    }

    public function before_create($deferred_session_key = null)
    {
      $this->token = $this->generate_token($this->name);
    }

    // CLASS METHODS
    public static function all_who_qualify_for_payout() 
    {
      $affiliates = self::create()->find_all();
      $qualified = array();
      $settings = ThreeTwentyNYAffiliate_Setting::get();
      foreach($affiliates as $affiliate) {
        if ($affiliate->new_commission() > $settings->payout_minimum) {
          array_push($qualified, $affiliate);
        }
      }
      return $qualified;
    }

    // METHODS
    public function generate_token($name) {
      return sha1(uniqid($name));
    }

    public function qualified_referrals() {
      $setting = ThreeTwentyNYAffiliate_Setting::get();
      $obj = new ThreeTwentyNYAffiliate_Referral();
      $obj->join('shop_orders', 'threetwentynyaffiliate_affiliate_orders.order_id = shop_orders.id');
      $obj->where('threetwentynyaffiliate_affiliate_orders.affiliate_id=?', $this->id);
      $setting_ids = $setting->order_status_ids();
      $ids = strlen($setting_ids) > 0 ? $setting_ids : '0';
      $obj->where("shop_orders.status_id IN (" . $ids . ")");
      return $obj->order("created_at desc")->find_all();
    } 

    public function referrals($status=null)
    // Returns a list of referrals filtered by an optional status.
    // If no status is supplied all qualified referrals will be returned 
    {
      $qualified_referrals = $this->qualified_referrals();
      if($status) {
        $filtered_referrals = array();
        foreach($qualified_referrals as $referral) {
          if (strtolower($referral->status) == strtolower($status)) {
           array_push($filtered_referrals, $referral);
          }
        }
        return $filtered_referrals; 
      } else {
        return $qualified_referrals;
      }
    }

    // Form methods
    public function get_commission_method_options($key_index = -1)
    {
      $setting = ThreeTwentyNYAffiliate_Setting::get();
      return $setting->get_commission_method_options();
    }

    public function get_payment_state_options($key_value = -1)
    {
      if ($key_value != -1)
      {
        if (!strlen($key_value))
          return null;
          
        $obj = Shop_CountryState::create()->find($key_value);
        return $obj ? $obj->name : null;
      }
      return $this->list_states($this->payment_country_id);
    }

    public function list_states($country_id)
    {
      // If no country is found return the first one on the DB ordered by name
      if (!$country_id || !Shop_Country::create()->find($country_id))
      {
        $obj = Shop_Country::create()->order('name')->find();
        if ($obj)
          $country_id = $obj->id;
      }
       
      $states = Db_DbHelper::objectArray(
        'select * from shop_states where country_id=:country_id order by name',
        array('country_id'=>$country_id)
      );
      
      $result = array();
      foreach ($states as $state)
        $result[$state->id] = $state->name;
        
      if (!count($result))
        $result = array(null=>'<no states available>');

      return $result;
    }

    public function new_commission() 
    {
      $total = 0.0;
      foreach($this->referrals('new') as $referral) {
        $total = $total + $referral->commission;
      }
      return $total;
    }
   
  }
