<?php
  class ThreeTwentyNYAffiliate_Referral extends Db_ActiveRecord
  {
    public $table_name = 'threetwentynyaffiliate_affiliate_orders'; 

    // For created_at and updated_at fields
    public $implement = 'Db_AutoFootprints';
    public $auto_footprints_visible = true;

    public $belongs_to = array(
      'affiliate'=>array('class_name'=>'ThreeTwentyNYAffiliate_Affiliate', 'foreign_key'=>'affiliate_id'),
      'order'=>array('class_name'=>'Shop_Order', 'foreign_key'=>'order_id'),
      'payout'=>array('class_name'=>'ThreeTwentyNYAffiliate_Payout', 'foreign_key'=>'payout_id')
    );

    public static function create()
    {
      return new self();
    }

    // SCHEMA CONFIGURATION
    public $custom_columns = array('commission'=>db_float, 'status'=>db_varchar);

    public function define_columns($context = null)
    {
      $this->define_column('id', '#');
      $this->define_column('status', 'Payment Status')->type(db_varchar);
      $this->define_column('commission_method', 'Commission Method');
      $this->define_column('commission_percentage', 'Percentage')->validation()->fn('trim')->required('Please specify a percentage')->method('validates_commission_percentage');
      $this->define_column('commission_flat_rate', 'Flat Rate')->validation()->fn('trim')->required('Please specify a flat rate');
      $this->define_column('commission', 'Commission')->currency(true)->type(db_varchar);
    }

    // Form
    public function define_form_fields($context = null)
    {
      if ($context == 'preview') {
        $this->add_form_field('status', 'left')->tab('Referral Details');
        $this->add_form_field('commission_method', 'left')->tab('Referral Commission Settings');
        if ($this->commission_method == 'percentage') {
          $this->add_form_field('commission_percentage', 'left')->tab('Referral Commission Settings');
        } else {
          $this->add_form_field('commission_flat_rate', 'left')->tab('Referral Commission Settings');
        }
        $this->add_form_field('commission', 'left')->tab('Referral Details');
      } else {
        $this->add_form_field('commission_method', 'left')->tab('Referral Commission Settings')->renderAs(frm_dropdown);
        $this->add_form_field('commission_percentage', 'left')->tab('Referral Commission Settings');
        $this->add_form_field('commission_flat_rate', 'left')->tab('Referral Commission Settings');
      }
    }

    // Validations
    public function validates_commission_percentage($name, $value)
    {
      if ($value > 100.0 || $value < 0)
        $this->validation->setError('Percentage must be between 0 and 100.', $name, true);

      return $value;
    }

    // Virtual Attributes
    public function eval_commission() {
      $commission = 0;
      if ($this->commission_method == 'percentage') {
        $commission = $this->commission_percentage * 0.01 * $this->order->subtotal;
      } else {
        // Must be flat_rate
        $commission = $this->commission_flat_rate;
      }
      $api_result = Backend::$events->fireEvent('threetwentynyaffiliate:onGetReferralCommission', $this, $commission);
      foreach($api_result as $value)
      {
        if(strlen($value)) {
          $commission = $value;
          break;
        }
      } 
      return $commission;
    }

    public function eval_status() {
      if ($this->payout) {
        return ($this->payout->status == 'paid') ? 'paid_out' : 'processing';
      } else {
        return 'new';
      }
    }

    // Form methods
    public function get_commission_method_options($key_index = -1)
    {
      $setting = ThreeTwentyNYAffiliate_Setting::get();
      return $setting->get_commission_method_options();
    }

    // METHODS
    public function status_color() 
    {
      switch ($this->status) {
        case 'new':
          return '#ff0000';
        case 'processing':
          return '#f8ff01';
        case 'paid_out':
          return '#32cd32';
      }
    }

  }
