<?php
  class ThreeTwentyNYAffiliate_Payout extends Db_ActiveRecord
  {
    public $table_name = 'threetwentynyaffiliate_payouts'; 

    // For created_at and updated_at fields
    public $implement = 'Db_AutoFootprints';
    public $auto_footprints_visible = true;

    public $belongs_to = array(
      'affiliate'=>array('class_name'=>'ThreeTwentyNYAffiliate_Affiliate', 'foreign_key'=>'affiliate_id')
    );

    public $has_many = array(
      'referrals'=>array('class_name'=>'ThreeTwentyNYAffiliate_Referral', 'foreign_key'=>'payout_id', 'order'=>'created_at desc')
    );

   
    public static function create()
    {
      return new self();
    }

    // SCHEMA CONFIGURATION
    public $custom_columns = array('amount_owed'=>db_float, 'display_status'=>db_varchar);

    public function define_columns($context = null)
    {
      $this->define_column('status', 'Status')->invisible();
      $this->define_column('id', '#');
      $this->define_column('display_status', 'Status');
      $this->define_column('amount_paid', 'Amount Paid')->currency(true);
      $this->define_column('amount_owed', 'Amount Owed')->currency(true);
      $this->define_column('payment_method', 'Payment Method');
      $this->define_relation_column('affiliate', 'affiliate', 'Affiliate', db_number, '@name')->validation()
        ->method('validates_affiliate_has_unpaid_commission')->method('validates_payout_meets_minimum');  
    }

    public function define_form_fields($context = null)
    {
      if ($this->new_record) {
        $this->add_form_field('affiliate');
      } else {
        $this->add_form_field('status')->renderAs(frm_dropdown);
      }
      if ($context == 'preview') {
        $this->add_form_field('amount_owed');
      }
    }

    // VIRTUAL ATTRIBUTES
    public function eval_amount_owed() 
    {
      $total = 0.0;
      foreach($this->referrals as $referral) {
        $total = $total + $referral->commission;
      }
      return $total - $this->amount_paid;
    }

    public function eval_display_status()
    {
      return ucfirst($this->status);
    }

    // VALIDATIONS
    public function validates_affiliate_has_unpaid_commission($name, $value)
    {
      if ($this->new_record && !$this->affiliate->referrals('new')) {
        $this->validation->setError('The selected affiliate has no new referrals.', $name, true);
      } else {
        return $value;
      }
    }

    public function validates_payout_meets_minimum($name, $value) 
    {
      $setting = ThreeTwentyNYAffiliate_Setting::get();
      $new_commission = $this->affiliate->new_commission();
      if ($new_commission < $setting->payout_minimum) {
        $this->validation->setError("The affiliate's new commmission of ". format_currency($new_commission) .
          " is less than the required minimum of ".format_currency($setting->payout_minimum), $name, true);
      } else {
        return $value;
      }
    }
    
    // Form methods
    public function get_status_options($key_index = -1)
    {
      return array(
        'unpaid'=>'Unpaid',
        'paid'=>'Paid'
      );
    }

    // METHODS
    public function assign_new_referrals()
    {
      foreach($this->affiliate->referrals('new') as $referral) {
        $referral->payout_id = $this->id;
        $referral->save();
      }
    }

    public function status_color()
    {
      return ($this->is_paid()) ? "#32cd32" : "#ff0000";
    }

    public function is_paid()
    {
      return ($this->status == 'paid') ? true : false;
    }

    // CALLBACKS
    public function before_update($deferred_session_key = null) 
    {
      if ($this->is_paid()) {
        $this->amount_paid = $this->amount_owed;
      } else {
        $this->amount_paid = 0.0;
      }
    }

    public function before_create($deferred_session_key = null)
    {
      $settings = ThreeTwentyNYAffiliate_Setting::get();
      $this->amount_paid = 0.0;
      $this->payment_method = $settings->payout_payment_method;
    }

  }
