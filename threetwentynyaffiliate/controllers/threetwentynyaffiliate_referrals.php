<?php
  class ThreeTwentyNYAffiliate_Referrals extends Backend_Controller
  {
    public $implement = 'Db_FormBehavior';

    // Form Settings
    public $form_model_class = 'ThreeTwentyNYAffiliate_Referral';
    public $form_not_found_message = 'Affiliate Order not found';
    public $form_redirect = null;

    // Preview Settings
    public $form_preview_title = 'Affiliate Order';

    // Edit Settings
    public $form_edit_title = 'Edit Affiliate Order';
    public $form_edit_save_auto_timestamp = true;
    public $form_edit_save_flash = 'The affiliate order has been successfully updated';
    public $form_edit_save_redirect = null;

    // Delete Settings
    public $form_delete_redirect = null;
    public $form_edit_delete_flash = 'The affiliate order has been successfully deleted';

    public function __construct() 
    {
      parent::__construct();
      // Page Title
      $this->app_module_name = 'Affiliate Program';
      // Menu
      $this->app_tab = 'threetwentynyaffiliate';
      $this->app_page = 'affiliates';
      // Redirect Settings
      $this->form_redirect = url('/threetwentynyaffiliate/affiliates');
      $this->form_delete_redirect = url('/threetwentynyaffiliate/affiliates');
      $this->form_edit_save_redirect = url('/threetwentynyaffiliate/referrals/preview/%s/?'.uniqid());
      $this->form_create_save_redirect = url('/threetwentynyaffiliate/referrals/preview/%s/?'.uniqid());
    }

    function preview_onDeleteAffiliateOrder($record_id)
    {
      try
      {
        $affiliate_order = ThreeTwentyNYAffiliate_Referral::create()->find($record_id);
        if (!$affiliate_order)
          throw new Phpr_ApplicationException('Affiliate order with identifier '.$record_id.' not found.');

        $affiliate_id = $affiliate_order->affiliate->id;
        $affiliate_order->delete();

        Phpr::$session->flash['success'] = $this->form_edit_delete_flash;
        Phpr::$response->redirect(url('threetwentynyaffiliate/affiliates/preview/'. $affiliate_id . '?' . uniqid()));
      }
      catch (Exception $ex)
      {
        Phpr::$response->ajaxReportException($ex, true, true);
      }
    }
  }
