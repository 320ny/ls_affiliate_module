<?php

  class ThreeTwentyNYAffiliate_Settings extends Backend_SettingsController
  {
    protected $access_for_groups = array(Users_Groups::admin);
    public $implement = 'Db_FormBehavior';

     // Form Settings
    public $form_edit_title = 'Affiliate Program Settings';
    public $form_model_class = 'ThreeTwentyNYAffiliate_Setting';

    public $form_redirect = null;
    public $form_edit_save_flash = "Affiliate Program settings have been saved.";

    public function __construct()
		{
			parent::__construct();
      $this->app_tab = 'system';

      $this->form_redirect = url('system/settings/');
		}
		
    // GET {App Backend}/threetwentynyaffiliate/settings
		public function index()
		{
			try
			{
		    $record = ThreeTwentyNYAffiliate_Setting::get();	
        if (!$record)
          throw new Phpr_ApplicationException('Affiliate Program configuration is not found.');

        $this->edit($record->id);
        $this->app_page_title = $this->form_edit_title;
      }
			catch (exception $ex)
			{
				$this->handlePageError($ex);
			}
		}

    public function index_onSave()
    {
      $record = ThreeTwentyNYAffiliate_Setting::get();
      $this->edit_onSave($record->id);
     }
  }
