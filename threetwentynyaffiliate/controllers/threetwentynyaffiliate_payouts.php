<?php
  class ThreeTwentyNYAffiliate_Payouts extends Backend_Controller
  {
    public $implement = 'Db_ListBehavior, Db_FormBehavior, Db_FilterBehavior';

    // List Settings
    public $list_model_class = 'ThreeTwentyNYAffiliate_Payout';
    public $list_record_url = null;
    public $list_search_enabled = true;
    public $list_search_fields = array('@id', '@status', 'affiliate_calculated_join.name');
    public $list_search_prompt = 'search by id, status, or affiliate name';
    public $list_top_partial = 'payout_selectors';

    // Form Settings
    public $form_model_class = 'ThreeTwentyNYAffiliate_Payout';
    public $form_not_found_message = 'Payout not found';
    public $form_redirect = null;

    // Preview Settings
    public $form_preview_title = 'Payout';

    // Edit Settings
    public $form_edit_title = 'Edit Payout';
    public $form_edit_save_auto_timestamp = true;
    public $form_edit_save_flash = 'The payout has been successfully saved';
    public $form_edit_save_redirect = null;

    // Create Settings
    public $form_create_title = 'New Payout';
    public $form_create_save_flash = 'A new payout has been created';
    public $form_create_save_redirect = null;

    // Delete Settings
    public $form_delete_redirect = null;
    public $form_edit_delete_flash = 'The payout has been successfully deleted';

    public function __construct()
    {
      parent::__construct();
      // Page Title
      $this->app_module_name = 'Affiliate Program';
      // Menu
      $this->app_tab = 'threetwentynyaffiliate';
      $this->app_page = 'payouts';
      $this->list_record_url = url('/threetwentynyaffiliate/payouts/preview/');
      // Redirect Settings
      $this->form_redirect = url('/threetwentynyaffiliate/payouts');
      $this->form_delete_redirect = url('/threetwentynyaffiliate/payouts');
      $this->form_edit_save_redirect = url('/threetwentynyaffiliate/payouts/preview/%s/?'.uniqid());
      $this->form_create_save_redirect = url('/threetwentynyaffiliate/payouts/preview/%s/?'.uniqid());
    }

    // GET {App Backend}/threetwentynyaffiliate/payouts
    public function index()
    {
      $this->app_page_title = 'Payouts';
    }

    // POST threetwentynyaffiliate/payouts
    public function formAfterCreateSave($model, $session_key)
    {
      $model->assign_new_referrals();
    }   

    public function preview_onDelete($record_id)
    {
      try
      {
        $payout = ThreeTwentyNYAffiliate_Payout::create()->find($record_id);
        if (!$payout)
          throw new Phpr_ApplicationException('Payout with identifier '.$record_id.' not found.');

        $payout->delete();

        Phpr::$session->flash['success'] = $this->form_edit_delete_flash;
        Phpr::$response->redirect(url('threetwentynyaffiliate/payouts'));
      }
      catch (Exception $ex)
      {
        Phpr::$response->ajaxReportException($ex, true, true);
      }
    }

    /*
     * STATUS CHANGE
    */

    // GET threetwentynyaffiliate/payouts/change_status/:id
    public function change_status($payout_id) 
    {
      $this->app_page_title = 'Change Order Status';

      try
      {
        $payout = ThreeTwentyNYAffiliate_Payout::create()->find($payout_id);
        $payout->define_form_fields();
        if (!$payout)
          throw new Phpr_ApplicationException('Payout not found');

        $this->viewData['payout'] = $payout;
      } 
      catch (Exception $ex)
      {
        $this->handlePageError($ex);
      }

    }

    // POST threetwentynyaffiliate/payouts/change_status/:id
    public function change_status_onSave($payout_id)
    {
      try
      {
        $payout = ThreeTwentyNYAffiliate_Payout::create()->find($payout_id);
        if (!$payout)
          throw new Phpr_ApplicationException('Payout not found');

        $data = post('ThreeTwentyNYAffiliate_Payout', array());
        $payout->status = $data['status'];
        $payout->save();

        Phpr::$session->flash['success'] = 'Payout status has been successfully changed';
        Phpr::$response->redirect(url('/threetwentynyaffiliate/payouts/preview/'.$payout_id));
      }
      catch (Exception $ex)
      {
        Phpr::$response->ajaxReportException($ex, true, true);
      }
    }

    /*
     * MASS GENERATE PAYOUTS
    */

    // POST threetwentynyaffiliate/payouts
    //  PHPR-EVENT-HANDLER: ev{index_onLoadMassGeneratePayoutsForm}
    public function index_onLoadMassGeneratePayoutsForm()
    {
      $settings = ThreeTwentyNYAffiliate_Setting::get();
      $this->viewData['payout_minimum'] = $settings->payout_minimum;
      $this->viewData['payout_count'] = count(ThreeTwentyNYAffiliate_Affiliate::all_who_qualify_for_payout());

      $this->renderPartial('mass_generate_payouts_form');
    }

    // POST threetwentynyaffiliate/payouts
    //  PHPR-EVENT-HANDLER: ev{index_onMassGeneratePayouts}
    public function index_onMassGeneratePayouts()
    {
      $payouts_processed = 0;
      $affiliates = ThreeTwentyNYAffiliate_Affiliate::all_who_qualify_for_payout();
      foreach($affiliates as $affiliate) {
        try 
        {
          // Create new Payout
          $payout = ThreeTwentyNYAffiliate_Payout::create();
          // Assign Affiliate
          $payout->affiliate = $affiliate;
          $payout->save();
          $payout->assign_new_referrals();
        }
        catch (Exception $ex)
        {
          Phpr::$session->flash['error'] = $ex->getMessage();
          break;
        }
      }
      if ($payouts_processed) {
        if ($payouts_processed > 1)
          Phpr::$session->flash['success'] = $payouts_processed . " payouts have been successfully generated.";
        else
          Phpr::$session->flash['success'] = "1 payout has been successfully generated.";
      }

      Phpr::$response->redirect(url('threetwentynyaffiliate/payouts').'?'.uniqid());
    }

    // POST threetwentynyaffiliate/payouts
    //   PHPR-EVENT-HANDLER: ev{index_onLoadChangeStatusForm}
    public function index_onLoadChangeStatusForm()
    {
      $this->viewData['payout_ids'] = post('list_ids', array());
      $this->renderPartial('change_status_form');
    }

    // POST threetwentynyaffiliate/payouts
    //
    public function index_onSetOrderStatuses()
    {
      $payouts_processed = 0;
      $data = post('ThreeTwentyNYAffiliate_Payout', array());
      $payout_ids = explode(',', $data['payout_ids']);
      $status = trim($data['status']);

      foreach ($payout_ids as $payout_id)
      {
        $payout_id = trim($payout_id);
        try
        {
          $payout = ThreeTwentyNYAffiliate_Payout::create()->find($payout_id);
          if(!$payout)
            throw new Phpr_ApplicationException('Payout #'.$payout_id.' not found');
          $payout->status = $status;
          $payout->save();
          $payouts_processed++;
        }
        catch (Exception $ex)
        {
          Phpr::$session->flash['error'] = $ex->getMessage();
          break;
        }
      }

      if($payouts_processed)
      {
        if($payouts_processed > 1)
          Phpr::$session->flash['success'] = $payouts_processed.' payouts have been successfully updated to a status of '.ucwords($status).'.';
        else
          Phpr::$session->flash['success'] = '1 payout has been successfully updated to a status of '.ucwords($status).'.';
      }

      Phpr::$response->redirect(url('threetwentynyaffiliate/payouts').'?'.uniqid());
    }

    // List methods
    public function listGetRowClass($model)
    {
      $classes = ' payout_status_'.$model->status;
      return $classes;
    }

  }
