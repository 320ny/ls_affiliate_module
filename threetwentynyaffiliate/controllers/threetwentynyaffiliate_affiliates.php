<?php
  class ThreeTwentyNYAffiliate_Affiliates extends Backend_Controller
  {
    public $implement = 'Db_ListBehavior, Db_FormBehavior, Db_FilterBehavior';

    // List Settings
    public $list_model_class = 'ThreeTwentyNYAffiliate_Affiliate';
    public $list_record_url = null;
    public $list_search_enabled = true;
    public $list_search_fields = array('@name');
    public $list_search_prompt = 'find affiliates by name';

    // Form Settings
    public $form_model_class = 'ThreeTwentyNYAffiliate_Affiliate';
    public $form_not_found_message = 'Affiliate not found';
    public $form_redirect = null;

    // Preview Settings
    public $form_preview_title = 'Affiliate';

    // Edit Settings
    public $form_edit_title = 'Edit Affiliate';
    public $form_edit_save_auto_timestamp = true;
    public $form_edit_save_flash = 'The affiliate has been successfully saved';
    public $form_edit_save_redirect = null;

    // Create Settings
    public $form_create_title = 'New Affiliate';
    public $form_create_save_flash = 'The affiliate has been successfully added';
    public $form_create_save_redirect = null;
    
    // Delete Settings
    public $form_delete_redirect = null;
    public $form_edit_delete_flash = 'The affiliate has been successfully deleted';

    public $globalHandlers = array(
      'onPaymentCountryChange'
    );

    public function __construct()
    {
      parent::__construct();
      // Page Title
      $this->app_module_name = 'Affiliate Program';
      // Menu
      $this->app_tab = 'threetwentynyaffiliate';
      $this->app_page = 'affiliates';
      $this->list_record_url = url('/threetwentynyaffiliate/affiliates/preview/');
      // Redirect Settings
      $this->form_redirect = url('/threetwentynyaffiliate/affiliates');
      $this->form_delete_redirect = url('/threetwentynyaffiliate/affiliates');
      $this->form_edit_save_redirect = url('/threetwentynyaffiliate/affiliates/preview/%s/?'.uniqid());
      $this->form_create_save_redirect = url('/threetwentynyaffiliate/affiliates/preview/%s/?'.uniqid());
    }

    // GET {App Backend}/threetwentynyaffiliate/affiliates
    public function index()
    {
      $this->app_page_title = 'Affiliates';
    }
 
    public function preview_onDeleteAffiliate($record_id)
    {
      try
      {
        $affiliate = ThreeTwentyNYAffiliate_Affiliate::create()->find($record_id);
        if (!$affiliate)
          throw new Phpr_ApplicationException('Affiliate with identifier '.$record_id.' not found.');

        $affiliate->delete();

        Phpr::$session->flash['success'] = $this->form_edit_delete_flash;
        Phpr::$response->redirect(url('threetwentynyaffiliate/affiliates'));
      }
      catch (Exception $ex)
      {
        Phpr::$response->ajaxReportException($ex, true, true);
      }
    }
    
    // POST threetwentynyaffiliate/affiliate/edit/:id
    //   PHPR-EVENT-HANDLER: ev{onPaymentCountryChange}
    protected function onPaymentCountryChange()
    {
      $form_model = $this->formCreateModelObject();
     
      $data = post('ThreeTwentyNYAffiliate_Affiliate');
      $form_model->payment_country_id = $data['payment_country_id'];
      echo ">>form_field_container_payment_state_idThreeTwentyNYAffiliate_Affiliate<<";
      $this->formRenderFieldContainer($form_model, 'payment_state');
    }
  }
